//
//  ViewController.h
//  SampleProject
//
//  Created by Ourangzaib khan on 2/12/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController  <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;

-(void) setTableViewdata;
@end

