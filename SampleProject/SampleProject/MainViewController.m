//
//  ViewController.m
//  SampleProject
//
//  Created by Ourangzaib khan on 2/12/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import "MainViewController.h"
#import "TableInfoCell.h"
#import "Service Layer/Service.h"
@interface MainViewController ()
@end

@implementation MainViewController
 NSMutableArray *values;
- (void)viewDidLoad {
    [super viewDidLoad];    
     self.navigationController.title = @"Web Info";
    values = [[NSMutableArray alloc] init];
   }
-(void)viewWillAppear:(BOOL)animated{
    [self setTableViewdata];

}

- (void)didReceiveMemoryWarning {
[super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
-(void) setTableViewdata{

    Service *service = [Service sharedInstance];
    [service getinfo : ^void(NSMutableArray *array, NSError * error){
           values = array;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}

//#pragma mark tableview data sources

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableInfoCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    
    if ( cell == NULL){
        cell = [[TableInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];  ;
        
    }else{
        [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
    }
    
    NSDictionary *dictionary = [values objectAtIndex:indexPath.row];
    
    cell.date.text =  [[dictionary valueForKey:@"high"] stringValue];
    cell.volume.text = [[dictionary valueForKey:@"volume"] stringValue];
    cell.Open.text = [[dictionary valueForKey:@"open"] stringValue];
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (values.count > 0) {
       return [values count];
    }
    return 0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



@end
