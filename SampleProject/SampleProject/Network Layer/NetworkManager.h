//
//  NetworkManager.h
//  SampleProject
//
//  Created by Ourangzaib khan on 2/12/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AFNetworking;
#import "AFNetworking.h"
typedef void (^CompletionBlock)(NSMutableArray *, NSError *);
@interface NetworkManager : NSObject
- ( void )getinfos :  (CompletionBlock)completionBlock;

@end
