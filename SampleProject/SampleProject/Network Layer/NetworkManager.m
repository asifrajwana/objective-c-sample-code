//
//  NetworkManager.m
//  SampleProject
//
//  Created by Ourangzaib khan on 2/12/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import "NetworkManager.h"

@implementation NetworkManager



- ( void )getinfos : (CompletionBlock)completionBlock{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSURL *URL = [NSURL URLWithString:@"http://chartapi.finance.yahoo.com/instrument/1.0/GOOG/chartdata;type=quote;range=1m/json/"
                  ];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSString* myString;
            myString = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];
            
            NSRange range = [myString rangeOfString:@"{"];
            range.length = myString.length - range.location - 2;
            
                        myString = [myString substringFromIndex:range.location];
             myString = [myString substringToIndex:myString.length - 2];
            
            NSData *data = [myString dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSMutableArray * aray = json[@"series"];
            completionBlock(aray,nil);
        }
    }];
    [dataTask resume];
}
@end
