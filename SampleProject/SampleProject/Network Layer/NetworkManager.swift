//
//  NetworkManager.swift
//  Sample Project
//
//  Created by Ourangzaib khan on 2/9/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import JSONPatchSwift

class NetworkManager: NSObject {

    
    func siteInfo(completionHandler: (NSMutableArray?, NSError?) -> ()) -> () {
        
        Alamofire.request(.GET, UrlConstants().siteUrl
            )
            .responseString { response in
                
                let seriesArray = self.jsonpToMutableArray(response.result.value!)
         
                completionHandler(seriesArray, nil)
                
                }
    }
    
    
    func jsonpToMutableArray (value : String) -> NSMutableArray{
        let string = value
        let range = string.rangeOfString("{")
        let myRange = Range<String.Index>(start: (range?.startIndex)!, end:  string.startIndex.advancedBy((string.characters.count) - 2) )
        var series = NSMutableArray()
        let substring   = string.substringWithRange(myRange)
        do {
            let data = substring.dataUsingEncoding(NSUTF8StringEncoding)
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
             series = json["series"] as! NSMutableArray
            
        } catch {
            
        }
        return series;
        
    }
    
    
}

