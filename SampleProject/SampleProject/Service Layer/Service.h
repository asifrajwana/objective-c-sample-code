//
//  Service.h
//  SampleProject
//
//  Created by Ourangzaib khan on 2/12/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkManager.h"
@interface Service : NSObject
+ (Service *)sharedInstance;
-(void) getinfo : (CompletionBlock)completionBlock;
@end
