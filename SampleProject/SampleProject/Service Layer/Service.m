//
//  Service.m
//  SampleProject
//
//  Created by Ourangzaib khan on 2/12/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

#import "Service.h"

@implementation Service
NetworkManager *networkmanager;

- (id)init
{
    self = [super init];
    if (self) {
        networkmanager = [[NetworkManager alloc] init];
    }
    return self;
}


+ (Service*)sharedInstance
{
    static Service *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[Service alloc] init];
    });
    return _sharedInstance;
}

-(void) getinfo:(CompletionBlock)completionBlock {
    [networkmanager getinfos:completionBlock];
   }

@end

